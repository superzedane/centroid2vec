from neo4j import GraphDatabase

def open(uri,user,password):
    return GraphDatabase.driver(uri, auth=(user, password))

def close(driver):
    driver.close()

def deleteAll(driver):
        with driver.session() as session:
                session.run("MATCH (node) DETACH DELETE node")

def addVocab(driver, worda, wordb):
        with driver.session() as session:
                session.run("MERGE (vocaba:VOCAB{name : $worda}) ON CREATE SET vocaba.count = 1 ON MATCH SET vocaba.count = (vocaba.count + 1) MERGE (vocabb:VOCAB{name : $wordb}) ON CREATE SET vocabb.count = 1 ON MATCH SET vocabb.count = (vocabb.count + 1) MERGE (vocaba)-[link:LINK]-(vocabb) ON CREATE SET link.count = 1 ON MATCH SET link.count = (link.count + 1) ",
                                 worda=worda, wordb=wordb)

def addDistanceLink(driver, threshold=1, formula='1.0/link.count'):
        with driver.session() as session:
                        session.run("MATCH ()-[dist:DISTANCE]-() DELETE dist")
                        session.run("MATCH (w1)-[link:LINK]-(w2)  WHERE link.count > $threshold MERGE (w1)-[dist1:DISTANCE]->(w2) SET dist1.weight = " + formula + " MERGE (w2)-[dist2:DISTANCE]->(w1) SET dist2.weight = " + formula, threshold=threshold)
                        session.run("MATCH ()-[dist:DISTANCE]-() WHERE dist.weight > 1.0 SET dist.weight = 1.0")

def getWords(driver):
        with driver.session() as session:
                return session.run("MATCH (w1)-[:DISTANCE]-() RETURN DISTINCT w1.name as name")
        

def print_greeting(driver, message):
    with driver.session() as session:
        greeting = session.write_transaction(create_and_return_greeting, message)
        print(greeting)

def create_and_return_greeting(tx, message):
    result = tx.run("CREATE (a:Greeting) "
                    "SET a.message = $message "
                    "RETURN a.message + ', from node ' + id(a)", message=message)
    return result.single()[0]

def findShortestPath(driver, worda, wordb):
        with driver.session() as session:
                results = session.run("MATCH (worda:VOCAB{name: $worda}), (wordb:VOCAB{name : $wordb}) CALL apoc.algo.dijkstra(worda, wordb, 'DISTANCE<', 'weight') YIELD path, weight RETURN path, weight", worda=worda, wordb=wordb)
                return results

def calShortestPath(driver, word, vocab_count):
        sum = 0.0
        count = 0
        members = []
        for vocab in list(vocab_count):
                results = findShortestPath(driver, word, vocab)
                weight = 0.0
                for record in results:
                        weight = record['weight']
                        count = count + 1
                        members.append(vocab)
                        break
                sum = sum + weight
        return { 'word' : word, 'weight' : sum, 'members' : members}


def calMinShortestPath(driver, words, vocab_count):
        min = 99999999
        minword = []
        paths = []
        for w in words:
                path = calShortestPath(driver, w, vocab_count)
                paths.append(path)
        max = 0
        for path in paths:
                if max < len(path['members']):
                        max  = len(path['members'])

        for path in paths:
                if len(path['members']) == max:
                        if path['weight']>0.0 and path['weight']<=min:
                                if path['weight']<min:
                                        minword = []
                                min = path['weight']
                                minword.append(path['word'])
        
        #no link between node select the most frequency word
        # if min > 999999.0:
        #         return [vocab_count.most_common(1)[0][0]]
        
        return {'words': minword , 'weight' : min}

def count_iterable(i):
    return sum(1 for e in i)

def test():
        driver = open('bolt://localhost:7687', 'neo4j', 'centroid')
        results = findShortestPath(driver, 'visit', 'click')

        for record in results:
                weight = record['weight']
        close(driver)

test()

# DELETE
# MATCH (node)-[rel]-() DELETE node, rel
# MATCH (node) DELETE node
# MATCH (node) OPTIONAL MATCH (node)-[rel]-() DELETE node, rel
# MATCH (node) DETACH DELETE node

# MATCH ()-[dist:DISTANCE]-() DELETE dist
# MATCH ()-[link:LINK]-()

# MERGE
# MERGE (location:Location{name: 'San Francisco'})
# ON CREATE SET location.created_at = timestamp(), location.update_count = 0, location.created_by = 'Louis Sayers'
# ON MATCH SET location.updated_at = timestamp(), location.update_count = (location.update_count + 1)
# RETURN location

# MERGE (movie:Movie{titile : 'Pride'})
# MERGE (lily:Person{name: 'Lily'})
# MERGE (lily)-[role:ACTED_IN]->(movie)
# SET lily.born = 1989
# SET role.earnings = 900000, role.roles = ['Elizabeth']
# RETURN lily, movie

# MERGE (vocaba:VOCAB{name : 'a'}) ON CREATE SET vocaba.count = 1 ON MATCH SET vocaba.count = (vocaba.count + 1) MERGE (vocabb:VOCAB{name : 'b'}) ON CREATE SET vocabb.count = 1 ON MATCH SET vocabb.count = (vocabb.count + 1) MERGE (vocaba)-[link:LINK]-(vocabb) ON CREATE SET link.count = 1 ON MATCH SET link.count = (link.count + 1) RETURN vocaba, link, vocabb
# MATCH (w1)-[link:LINK]-(w2)  WHERE link.count > 1 MERGE (w1)-[dist:DISTANCE]-(w2) SET dist.distance = 1.0/link.count RETURN w1, dist, w2
# MATCH (w1)-[:DISTANCE]-() RETURN DISTINCT w1

# MATCH (worda:VOCAB{name: $worda}), (wordb:VOCAB{name : $wordb}) 
# CALL apoc.algo.dijkstra(worda, wordb, 'DISTANCE>', 'weight') YIELD path, weight 
# RETURN path, weight