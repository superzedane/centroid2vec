import os
import glob
import nltk
import collections
import sklearn.decomposition
import matplotlib.pyplot
import gensim.models
import gensim.scripts.glove2word2vec
import scipy
import random
import numpy as np
from gensim import utils
from numpy import zeros, dtype, float32 as REAL, ascontiguousarray, fromstring
from six.moves import range
from six import iteritems

def findCentroid4(model,vocab):
    listvocab = list(vocab)
    vector_model = model.wv[listvocab]
    vector = findCentroid1(model,vocab)
    centroids = findKNN(vector_model,vector,10)
    return listvocab[centroids[1][0]]


def findCentroid3(model,vocab):
    listvocab = list(vocab)
    vector_model = model.wv[listvocab]
    vector = findCentroid1(model,vocab)
    centroids = findKNN(vector_model,vector,10)
    for i in centroids[1]:
        print(listvocab[i])
    return listvocab[centroids[1][0]]

def findKNN(vector_model,vector,k=1):
    tree = scipy.spatial.KDTree(vector_model)
    return tree.query(vector,k)

#combined all vectors
def findCentroid2(model,vocab):
    vector = [0] * model.vector_size
    count = 0
    for w in list(vocab):
        v = model.wv[w]
        vector = vector + v
        count = count + 1
    vector = vector/count
    return vector

#combined all vectors
def findCentroid1(model,vocab):
    vector = [0] * model.vector_size
    count = 0
    for w in list(vocab):
        v = model.wv[w]
        vector = vector + vocab[w]*v
        count = count + vocab[w]
    vector = vector/count
    return vector

def findNoun(token):
    is_noun = lambda pos: 'NN' in pos[:2]
    return [word for (word, pos) in nltk.pos_tag(token) if is_noun(pos)] 


def findWordsFromVector(model,vector,topn=10):
    # vec = array(vector, dtype=float32)
    return model.wv.similar_by_vector(vector, topn=topn, restrict_vocab=None)

def removeCounterNoVocab(model,vocab):
    for k in list(vocab):
        try: 
            embedding_vector = model.wv[k] 
        except KeyError: 
            vocab.pop(k)


def removeCounterUnderThreshold(vocab,threshold = 1):
    for k in list(vocab):
        if vocab[k] <= threshold:
            vocab.pop(k)

def filterWords(vocab1, vocab2):
    return filter(lambda x: x in vocab1, vocab2)

def getGloveModel(num=50, restrict_vocabs=False):
    word2vec_output_file = 'glove.6B.' + str(num) +'d.txt.word2vec'
    if os.path.isfile(word2vec_output_file) == False:
        glove_input_file = 'glove.6B.' + str(num) + 'd.txt'
        gensim.scripts.glove2word2vec.glove2word2vec(glove_input_file, word2vec_output_file)
    if restrict_vocabs:
        return load_word2vec_format(word2vec_output_file, binary=False)
    else:
        return gensim.models.KeyedVectors.load_word2vec_format(word2vec_output_file, binary=False)
    

def calPCA(model,vocabs):
    X = model[vocabs]
    pca = sklearn.decomposition.PCA(n_components=2)
    return pca.fit_transform(X)

def plotScatter(result,vocabs,color='blue'):
    words = list(vocabs)
    for i, word in enumerate(words):
        matplotlib.pyplot.scatter(result[i, 0], result[i, 1],color=color,s=vocabs[word]*2)
        matplotlib.pyplot.annotate(word, xy=(result[i, 0], result[i, 1]),color=color)

def plotShow():
    matplotlib.pyplot.show()

def plotWord2Vec(model, vocabs):
    X = model[vocabs]
    pca = sklearn.decomposition.PCA(n_components=2)
    result = pca.fit_transform(X)
    # create a scatter plot of the projection
    matplotlib.pyplot.scatter(result[:, 0], result[:, 1],color='green')
    words = list(vocabs)
    for i, word in enumerate(words):
        matplotlib.pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
    matplotlib.pyplot.show()

def listFiles(path = '',ext = '*.txt'):
    return [f for f in glob.glob(path + ext, recursive=True)]

def getTextFromFile(filename):
    file = open(filename, 'rt')
    text = file.read()
    file.close()
    return text

def getFirstLine(filename):
    with open(filename) as f:
        return f.readline()
    return None

def cleanSetences(text):
    sentences = nltk.sent_tokenize(text)
    output = []
    for sentence in sentences:
        words = cleanDoc(sentence)
        words = findNoun(words)
        output.append(words)
    return output

def cleanSetences2(text,vocabs):
    sentences = nltk.sent_tokenize(text)
    output = []
    vocab_words = list(vocabs)
    for sentence in sentences:
        words = cleanDoc(sentence)
        words = findNoun(words)
        awords = [d for d in words if d in vocab_words]
        output.append(awords)
    return output


def cleanDoc(text):
    tokens = nltk.word_tokenize(text)
    tokens1 = [w.lower() for w in tokens]
    # remove all tokens that are not alphabetic
    tokens2 = [word for word in tokens1 if word.isalpha()]
    stop_words = set(nltk.corpus.stopwords.words('english'))
    tokens3 = [w for w in tokens2 if not w in stop_words]
    # porter = nltk.PorterStemmer()
    # tokens4 = [porter.stem(word) for word in tokens3]
    tokens5 = [word for word in tokens3 if len(word) > 1]
    return tokens5

def countWords(tokens,vocab=None):
    if vocab is None:
        vocab = collections.Counter()
    vocab.update(tokens)
    return vocab

def trainGenSim(sentences,filename='model.bin'):
    model = gensim.models.Word2Vec(sentences, min_count=1,workers=1,iter=10)
    model.save(filename)
    return model

def trainFasttext(sentences,filename='modelfasttext.bin'):
    model = gensim.models.Word2Vec(sentences, min_count=1,workers=1,iter=10)
    model.save(filename)
    return model

def restrictCounter(vocab_count, words):
    for w in list(vocab_count):
        if w not in words:
             vocab_count.pop(w)

def color():
    return '#'+ ('%06X' % random.randint(0, 0xFFFFFF))

def removeCounter(main_dict, th=1):
    return {w : main_dict[w] for w in main_dict if main_dict[w] > th}

def isNNWord(word):
    stop_words = set(nltk.corpus.stopwords.words('english'))
    is_noun = lambda n: True if n[0] == 'N' or n[0] == 'V' else False
    if word.isalpha() and  len(word) > 1:
        w = nltk.pos_tag([word])
        if word not in stop_words and is_noun(w[0][1]):
            return True
    return False

def cleanWord2Vec(model):
    tokens2 = [word for word in model.wv.vocab if word.isalpha()]
    stop_words = set(nltk.corpus.stopwords.words('english'))
    tokens3 = [w for w in tokens2 if not w in stop_words]
    # porter = nltk.PorterStemmer()
    # tokens4 = [porter.stem(word) for word in tokens3]
    tokens5 = [word for word in tokens3 if len(word) > 1]
    tokens6 = findNoun(tokens5)
    restrict_w2v(model, tokens6)

def restrict_w2v(w2v, restricted_word_set):
    new_vectors = []
    new_vocab = {}
    new_index2entity = []
    new_vectors_norm = []

    for i in range(len(w2v.vocab)):
        word = w2v.index2entity[i]
        vec = w2v.vectors[i]
        vocab = w2v.vocab[word]
        # vec_norm = w2v.vectors_norm[i]
        if word in restricted_word_set:
            vocab.index = len(new_index2entity)
            new_index2entity.append(word)
            new_vocab[word] = vocab
            new_vectors.append(vec)
            # new_vectors_norm.append(vec_norm)

    w2v.vocab = new_vocab
    w2v.vectors = new_vectors
    w2v.index2entity = new_index2entity
    w2v.index2word = new_index2entity
    # w2v.vectors_norm = new_vectors_norm


def load_word2vec_format(fname, fvocab=None, binary=False, encoding='utf8', unicode_errors='strict', limit=None, datatype=REAL):
    from gensim.models.keyedvectors import Vocab
    counts = None
    if fvocab is not None:
        logger.info("loading word counts from %s", fvocab)
        counts = {}
        with utils.smart_open(fvocab) as fin:
            for line in fin:
                word, count = utils.to_unicode(line).strip().split()
                counts[word] = int(count)

    # logger.info("loading projection weights from %s", fname)
    with utils.smart_open(fname) as fin:
        header = utils.to_unicode(fin.readline(), encoding=encoding)
        vocab_size, vector_size = (int(x) for x in header.split())  # throws for invalid file format
        if limit:
            vocab_size = min(vocab_size, limit)
        result = gensim.models.keyedvectors.Word2VecKeyedVectors(vector_size)
        result.vector_size = vector_size
        result.vectors = zeros((vocab_size, vector_size), dtype=datatype)

        def add_word(word, weights):
            word_id = len(result.vocab)
            if word in result.vocab:
                logger.warning("duplicate word '%s' in %s, ignoring all but first", word, fname)
                return
            if counts is None:
                # most common scenario: no vocab file given. just make up some bogus counts, in descending order
                result.vocab[word] = Vocab(index=word_id, count=vocab_size - word_id)
            elif word in counts:
                # use count from the vocab file
                result.vocab[word] = Vocab(index=word_id, count=counts[word])
            else:
                # vocab file given, but word is missing -- set count to None (TODO: or raise?)
                # logger.warning("vocabulary file is incomplete: '%s' is missing", word)
                result.vocab[word] = Vocab(index=word_id, count=None)
            result.vectors[word_id] = weights
            result.index2word.append(word)

        if binary:
            binary_len = dtype(REAL).itemsize * vector_size
            for _ in range(vocab_size):
                # mixed text and binary: read text first, then binary
                word = []
                while True:
                    ch = fin.read(1)
                    if ch == b' ':
                        break
                    if ch == b'':
                        raise EOFError("unexpected end of input; is count incorrect or file otherwise damaged?")
                    if ch != b'\n':  # ignore newlines in front of words (some binary files have)
                        word.append(ch)
                word = utils.to_unicode(b''.join(word), encoding=encoding, errors=unicode_errors)
                with utils.ignore_deprecation_warning():
                    # TODO use frombuffer or something similar
                    weights = fromstring(fin.read(binary_len), dtype=REAL).astype(datatype)
                if isNNWord(word):
                    add_word(word, weights)
        else:
            for line_no in range(vocab_size):
                line = fin.readline()
                if line == b'':
                    raise EOFError("unexpected end of input; is count incorrect or file otherwise damaged?")
                parts = utils.to_unicode(line.rstrip(), encoding=encoding, errors=unicode_errors).split(" ")
                if len(parts) != vector_size + 1:
                    raise ValueError("invalid vector on line %s (is this really the text format?)" % line_no)
                word, weights = parts[0], [datatype(x) for x in parts[1:]]
                if isNNWord(word):
                    add_word(word, weights)
    if result.vectors.shape[0] != len(result.vocab):
        # logger.info(
        #     "duplicate words detected, shrinking matrix size from %i to %i",
        #     result.vectors.shape[0], len(result.vocab)
        # )
        result.vectors = ascontiguousarray(result.vectors[: len(result.vocab)])
    assert (len(result.vocab), vector_size) == result.vectors.shape

    # logger.info("loaded %s matrix from %s", result.vectors.shape, fname)
    return result
