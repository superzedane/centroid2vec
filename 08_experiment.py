import centroid2vec
import centroid
import time

# list wiki files from a diretory
files = centroid2vec.listFiles('./wiki/')
files.sort()

# open neo4j driver
driver = centroid.open('bolt://localhost:7687', user='neo4j', password='centroid')

# collect sentences and all words from all files
allsentences = []
for filename in files:
    text = centroid2vec.getTextFromFile(filename)
    sentences = centroid2vec.cleanSetences(text)
    allsentences.extend(sentences)

# delete all components in database graph
centroid.deleteAll(driver)

# add words in database graph
for sentence in allsentences:
    for i, word in enumerate(sentence):
        if i>0:
            centroid.addVocab(driver, sentence[i-1],sentence[i])

# add new relationship between node for distance weight (dice), 
# relationship words < threshold (1) are not included
centroid.addDistanceLink(driver,1,'(2.0*link.count)/(w1.count+w2.count)')

# retreives all words that connected > threshold
results = centroid.getWords(driver)
graphwords = []
for record in results:
    graphwords.append(record['name'])

# Process for each file content
for filename in files:

    # read text from file
    text = centroid2vec.getTextFromFile(filename)
    # spilt to sentences
    sentences = centroid2vec.cleanSetences(text)

    # clean words, only nouns
    tokens = centroid2vec.cleanDoc(text)
    tokens = centroid2vec.findNoun(tokens)

    # create counter words
    vocab_count = centroid2vec.countWords(tokens)
    vocab_count_temp = vocab_count

    # remove not significant words; count < threshold (1)
    vocab_count = centroid2vec.removeCounter(vocab_count, 1)

    # filter out to only relationship words
    centroid2vec.restrictCounter(vocab_count, graphwords)

    # measure delay
    start_time = time.time()

    # find centroids
    centroids = centroid.calMinShortestPath(driver, graphwords, vocab_count)

    # print result 
    print(filename + ';' + centroid2vec.getFirstLine(filename).rstrip() + ";" + vocab_count_temp.most_common(1)[0][0] + ';' + ' '.join(centroids['words']) + ';' + str(centroids['weight']) + ";" + str(time.time() - start_time))

# close neo4j driver
centroid.close(driver)