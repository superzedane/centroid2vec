#### Clone

git clone https://superzedane@bitbucket.org/superzedane/centroid2vec.git

#### Installation and setting		
1. Install Anaconda included Python,Keras and Visual studio code (used python 3.5.x that compatible with the lastest keras)		
2. Install neo4j Desktop and APOC Plugin, created a graph database		
3. Load source code from https://bitbucket.org/superzedane/centroid2vec		
4. Load and extracted GolVe Pre-Trained Vectors from http://nlp.stanford.edu/data/glove.6B.zip and etxract at the same folder		
		
#### Files		
1. wiki		100 english articles data
2. 05_experiment.py		tested fasttext
3. 07_experiment.py		tested GloVe Pretrained
4. 08_experiment.py		tested Original Centroid, Graph is built with all articles in combination But peformance is bad
5. 09_experiment.py		tested Original Centroid, created a graph for each article only
6. centroid.py		Original Centroid Calculation (Neo4j - Co-currence graph)
7. centroid2vec.py		Word2Vec Centroid Calculation (GenSim)

#### Summary
1. Centroid		
2. Fasttext	Not good only 51% corrected	
3. GloVe	Quite good 90% correct	
