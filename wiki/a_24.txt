Super Mario 64
Super Mario 64
For the Nintendo DS enhanced remake, see Super Mario 64 DS.
Super Mario 64
Developer(s)
Nintendo EAD
Publisher(s)
Nintendo
Designer(s)
Shigeru Miyamoto
Series
Mario series
Release date(s)
June 23, 1996[1]
September 29, 1996
March 1, 1997
November 2003 (iQue)
Genre(s)
Platform
Mode(s)
Single player
Rating(s)
ESRB: K-A (Kids to Adults) (Nintendo 64), E (Everyone) (Wii Virtual Console)
Platform(s)
Nintendo 64
iQue
Virtual Console
Media
64 Mb (8 MB) cartridge, Flash card
Super Mario 64 is a video game for the Nintendo 64. It debuted in Japan on June 23, 1996, in North America on September 29, 1996, and in Europe on March 1, 1997.[2] Along with Pilotwings 64, it was one of the launch titles for the new console.[3] As the flagship killer game, it drove initial sales of the Nintendo 64, and has sold over 11 million copies in total.[4]
Super Mario 64 was the first 3D game in the Mario series, and though it was not the first-ever 3D platformer, it helped to define the genre, much as Super Mario Bros. defined the 2D sidescrolling platformer. Super Mario 64 was considered so revolutionary that many consider it to have set the standard for all later 3D platformer games and 3D games in general.[5][6][7]
In going from two to three dimensions, Super Mario 64 replaced the linear obstacle courses of traditional platform games with vast worlds in which the player must complete multiple and diverse missions, with an emphasis on exploration. While doing so, it managed to preserve the feel of earlier Mario games, including many of their gameplay elements and characters.[7] It is acclaimed by critics as one of the greatest video games of all time.[8]
Contents
1 Story
2 Gameplay
2.1 Controls
2.2 Setting
3 Levels
4 Development
5 Reception
5.1 Reviews
6 Impact and legacy
6.1 Remakes and sequels
6.2 Rumors
7 Notes and references
8 See also
9 External links
[edit]
Story
Super Mario 64 begins with a letter from Princess Peach inviting Mario to come to her castle for a cake she has baked for him. When he arrives, Mario learns that Bowser has invaded the castle and imprisoned the Princess and her servants within it using the power of the castle's 120 power stars. Many of the castle's paintings are portals to other realms, in which Bowser's minions keep watch over the stars. Mario searches for the portals, entering them to recover the stars. As he finds more stars, he can reach new places in the castle, eventually reaching Bowser when he recovers 70 stars. He defeats Bowser three times, rescuing Peach and restoring the power of the stars to the castle. Peach rewards Mario by baking the cake that she had promised him.[9][10]
[edit]
Gameplay
[edit]
Controls
Mario's abilities in Super Mario 64 are far more diverse than those of previous Mario games. He can walk, run, crouch, crawl, swim, climb, and jump at great heights or distances using the game controller's analog stick and buttons. As jumping was Mario's signature move in earlier games, particular attention was paid to this move. Special jumps can be executed by combining a regular jump with other actions, including the extra high double and triple jumps (jumping two and three times in a row, respectively), the long jump, and backflip. There are also special maneuvers, such as wall jumping (jumping from one wall to another in rapid succession to reach areas that would otherwise be too high).[9][10]
Mario has a number of physical attacks in addition to jumping. His basic attack is the punch, which becomes a jump kick when performed in mid-air. Attacking while running causes Mario to lunge forward. Crouching while in the air executes a power stomp (also called the ground pound). Crouching while running and then immediately attacking executes a slide kick, while crouching and moving the analog stick makes Mario crawl and enter small spaces. Mario can pick up and carry certain items, an ability which is used to solve various puzzles. Mario can also swim underwater at various speeds. His life slowly diminishes while underwater (representing how long he can hold his breath); he must find coins or air bubbles to replenish it, or return to the surface before drowning.[9]
[edit]
Setting
Super Mario 64 is set in Princess Peach's Castle, which consists of three floors, a basement, a moat, and a courtyard. The area outside the castle is an introductory area in which the player can experiment. Scattered throughout the castle are entrances to courses and other areas, usually accessed by jumping into a painting.
Each course is an enclosed world in which the player is free to wander in all directions and discover the environment without time limits. The worlds are filled with enemies that attack Mario as well as friendly creatures that provide assistance, offer information, or ask a favor. Mario gathers stars in each course; some stars only appear after Mario has completed certain tasks, often hinted at by the name of the course. These challenges include defeating a boss, solving puzzles, racing an opponent, and gathering coins. As Mario collects stars, more areas of the castle are opened. Mario unlocks doors in the castle with keys obtained by defeating Bowser in special courses.[9]
Mario is assisted in some courses by three cap power-ups. The Wing Cap allows Mario to fly, the Metal Cap makes him immune to most damage and allows him to withstand wind and walk underwater, and the Vanish Cap renders him partially immaterial and allows him to walk through some obstacles such as wire mesh.[9] Some courses contain cannons that Mario can unlock by speaking to a pink Bob-omb Buddy. After Mario enters a cannon, he can be shot out to reach distant places. When Mario has the Wing Cap, cannons can be used to reach high altitudes or fly across most of a level quickly.
There are many hidden secrets to the game, most containing extra stars needed to complete the game 100%.
[edit]
Levels
Super Mario 64 has fifteen main stages in total, and each is unique while incorporating elements of previous Mario video games.
The first floor (the one Mario arrives) has four levels:
Bob-omb Battlefield, a bright grassland following the tradition of first courses from earlier Mario games. The main enemies on this level are the Bob-omb. It also features a King Bob-omb.
Whomp's Fortress, a fortress floating in the sky filled with Piranha Plants and the Thwomps.
Jolly Roger Bay, a mostly-underwater course centered around a sunken pirate ship, also featuring a monstrously large eel.
Cool Cool Mountain, is a snow-themed course featuring some Penguins.[9]
Big Boo's Haunt, is located in the courtyard of the castle, and is also accessible from the first floor. The level is a haunted house with various Boos and other frightening enemies. [9]
The basement of the castle has four levels:
Hazy Mazy Cave, a complex of caverns, reminiscent of dungeons from earlier Mario games (and using a remix of the same music).
Lethal Lava Land, platforms above a sea of lava, as well as a volcano that Mario can enter.
Shifting Sand Land is a cruel desert (reminiscent of the desert courses in Super Mario Bros. 2 - also featuring Shy Guys and Pokeys - and Super Mario Bros. 3) that is home to a labyrinthine pyramid as well as a cap-stealing vulture named Klepto, the fearsome Eyerok and quicksand.
Dire Dire Docks, is another underwater course, involving two main areas separated by a tunnel and a submarine that belongs to Bowser.[9]
The second floor features four levels:
Snowman's Land, another snow-themed courses, centered around a giant climbable snowman mountain (that bizarrely enough, can talk.)
Wet-Dry World, a course in which Mario can raise and lower the water level. The initial water level corresponds to the height at which he enters the painting, and there are switches that can change the water level.
Tall Tall Mountain, an extremely steep mountain, that plays host to a cap-stealing monkey called Ukkiki as well as many precariously placed mushroom platforms;
Tiny-Huge Island can be played either as a small Mario in a world where everything is larger than normal, or as a large Mario in a world where everything is smaller than normal, a theme similar to World 4 of Super Mario Bros. 3. Its two differently sized paintings allow Mario to start the course either way, and warp pipes within the level allow him to switch.[9]
The third and final floor contains two levels:
Tick Tock Clock, the inside of a gigantic clock where Mario must navigate between moving parts such as pendulums and gears, and the speed and direction of the moving parts in this stage are affected by the positions of the hands of the clock when Mario jumps into it. This level was reused as a race track in Mario Kart DS.
Rainbow Ride, that takes place in the sky, with various platforms and floating buildings that can be reached by riding a magic carpet. The course's name, difficulty level and high altitude are reminiscent of the Rainbow Road courses from the Mario Kart games. This level was reused as an arena in Super Smash Bros. Melee under the name of "Rainbow Cruise". [9]
[edit]
Development
The development of Super Mario 64 took less than two years, but producer/director Shigeru Miyamoto had conceived of a 3D Mario game over five years before, while working on Star Fox.[12] Miyamoto developed most of the concepts during the era of the SNES and considered making it an SNES game (making use of the Super FX chip), but decided to develop it for the Nintendo 64 due to the earlier system's technical limitations.[13]
The development of Mario 64 started with the creation of the characters and camera system. Shigeru Miyamoto and the other designers were initially unsure of which direction the game should take, and months were spent selecting a camera view and layout that would be appropriate.[11] The original concept involved the game having fixed path much like an isometric type game, before the choice was made to settle on a free-roaming 3D design.[11] Although the majority of Mario 64 would end up featuring the free-roaming design, elements of the original fixed path concept would remain in certain parts of the game, particularly in the three Bowser encounters. One of the programmers for Mario 64, Giles Goddard, explained that these few linear elements survived as a means to force players into Bowser's lair rather than to encourage exploration.[11] The development team placed high priority on getting Mario's movements right, and before levels were created, the team were testing and refining Mario's animations on a simple grid. The first test scenario used to try out controls and physics involved Mario and the rabbit Mips, named for the MIPS processor in the Nintendo 64 (this scene remains as a minigame in the final game).
Shigeru Miyamoto's guiding design philosophy behind Super Mario 64 was to include more details.[12] Many were inspired from real life; for example, one character is based on director Takashi Tezuka's wife who "is very quiet normally, but one day she exploded, maddened by all the time [Tezuka] spent at work. In the game, there is now a character which shrinks when Mario looks at it, but when Mario turns away, it will grow large and menacing."[14] Super Mario 64 is also characterized by featuring more puzzles than earlier Mario games. It was developed simultaneously with The Legend of Zelda: Ocarina of Time, but as Zelda was released years later, some puzzles were taken from that game for Super Mario 64.[15]
Reliable information about Nintendo's new 3D Mario first leaked out in November 1995, and a playable version of Super Mario 64 was presented days later as part of the world premier for the Nintendo 64 (then known as Ultra 64) at Nintendo SpaceWorld. The basic controls had at this point been implemented, and the game was reportedly 50% finished, although most course design remained. At least 32 courses were planned, but the number turned out lower in the final game, as only 15 could fit (or 25 courses, if the games' 10 extra mini-levels are included).[12]
The music was composed by veteran Koji Kondo, who used new interpretations of the familiar melodies from earlier games as well as entirely new material. Thanks to an advanced compression technology using the Nintendo Ultra 64 Sound Format, which greatly reduced the memory space required for each music track and caused only a small loss in sound quality, the programmers were able to include a lengthy, near CD-quality soundtrack with more than 30 music tracks.
Super Mario 64 was one of the first in the series to feature the voice acting of Charles Martinet. It also features Leslie Swan as Princess Peach and Issac Marshall as Bowser (unlisted in credits). The characters speak more in the English version than in the Japanese version.[15] Sometimes different things are said, like Mario's "Bye bye" became "So long-a Bowser!". There are other differences, some of which remained in the English release of Super Mario 64 DS.[16]
[edit]
Reception
Super Mario 64 is often counted as one of the first games to have brought a series of 2D games into full 3D while maintaining their signature feel. The game was designed with the earlier Mario titles' maneuvers, power-up blocks, level themes (such as grassland, lava, ice, desert, and so on), enemies, and other characters in mind. Super Mario 64's translation of traditional 2D platforming action into 3D was hailed as a great success by many players, and the game itself went on to effectively drive sales of the N64 console.[7]
[edit]
Reviews
Super Mario 64 was praised in the gaming press, and is still highly acclaimed. It has collected numerous awards, including various "Game of the Year" honors by members of the gaming media, as well as Nintendo's own bestseller Player's Choice selection. It has placed high on many "greatest games of all time" lists, ranked #1 by Next Generation Magazine, #5 and #1 in Nintendo Power issues 200 and 100 respectively, #1 by Super PLAY, #5 by IGN,[17] #5 by Electronic Gaming Monthly, and #12 by GameInformer.[18] EGM awarded Super Mario 64 a Gold award in its initial review, and in Edge, the game was the first of only five games to ever score a perfect 10/10. GameSpot called Super Mario 64 one of the 15 most influential games of all time.[5]
[edit]
Impact and legacy
Super Mario 64 set many precedents for 3D platformers to follow.[7]
Unlike 2D games, 3D games must emulate a real perspective of characters and events. Most existing 3D games at the time used a first person or fixed perspective, but the platform gameplay of Super Mario 64 required the use of a free camera. The game world is therefore viewed through an in-game video camera operated by Lakitu.[10] Lakitu handles the camera automatically, but the player can change the perspective manually when necessary, since the camera programming occasionally makes the view get stuck behind walls or at odd angles. This was a useful innovation, as other games were sometimes unplayable due to an unfixable bad camera.[19]
The Nintendo 64's analog control stick allowed for more realistic and wide-ranging character movements than the digital D-pads of previous consoles, and Super Mario 64 exploits this feature extensively. For example, Mario's speed varies depending on the degree of tilt of the control stick.[10] The range and direction of many other movements can be controlled as well. The Bowser battles exhibit this by forcing the player to rotate the control stick in circles in order to swing Bowser around and throw him into mines placed around the arena.[20]
Super Mario 64 was also notable for its sense of freedom and non-linearity. This was initially unfamiliar to many people, among them voice actor Michael Grayford of Liquid Entertainment:
When I first played Mario 64, I was very turned off. There were too many places to run around and too much stuff to do, and I didn't really see the point or the spirit of the game. I tried it again later, though, hearing from everyone how fun it was, and ended up playing it all the way through to the end. I was highly pleased. Each level brought some new unique cool gameplay element and I was never bored.[21]
Warren Spector, former lead designer at Ion Storm Inc., gives the following explanation for the game's influence:
It's not possible to squeeze this much gameplay into a single game. Mario has, like, ten things he can do and yet there's never a moment where you feel constrained in any way. No game has done a better job of showing goals before they can be attained, allowing players to make a plan and execute on it. And the way the game allows players to explore the same spaces several times while revealing something new each time is a revelation. Any developer who wouldn't kill to have made this game is nuts.[21]
A central hub, where controls can be learned before entering levels themselves, has been used in many 3D platformers since. In addition, the game's mission-based level design was an inspiration for other game designers. For one example, Martin Hollis who produced and directed GoldenEye 007 says that "the idea for the huge variety of missions within a level came from Mario 64."[22]
[edit]
Remakes and sequels
Super Mario 64 was first re-released in Japan on 18 July 1997 as Shindou Super Mario 64. This version added support for the Rumble Pak and included voice acting from the American version.[23][24] In 1998, Super Mario 64 was re-released in America as part of the Player's Choice line: a selection of games with high sales sold for a reduced price.
A remake for the Nintendo DS called Super Mario 64 DS was available for the handheld system's launch in 2004. Yoshi, Luigi, and Wario were added as additional playable characters, and the game featured additional stars and courses, touch screen mini-games, and a few minor multiplayer modes.[25]
Super Mario 64 is downloadable for the Wii's Virtual Console service.[26] This release adds compatibility with the Gamecube and Classic controllers, and enhances the display by rendering polygons at approximately 4x the Nintendo 64's native 320x240 resolution. This version is able to run in 480p on properly configured setups.
Super Mario 64 2 was planned for the Nintendo 64DD, but canceled due to the failure of that peripheral, as well as a lack of progress in development. Super Mario Sunshine for the Nintendo GameCube built on Super Mario 64's core gameplay by adding a water pump device and add-on nozzles, similar to the Caps.
[edit]
Rumors
Because of Super Mario 64's great popularity, rumors about glitches and secrets spread rapidly after its release. The most infamous rumor is that Mario's brother Luigi is an unlockable character in the game. This rumor was fueled by a blurry text on the pedestal of a statue in the castle courtyard that supposedly read "L is Real 2041", which caused rampant fan speculation. (Upon closer inspection, the blurry texture appears to either be illegible, or read "Eternal Star.") IGN received so many questions and supposed methods to unlock Luigi that the staff offered a bounty to anyone who could prove that Luigi was in the game.[27] The number of false codes submitted to IGN dropped dramatically; no successful method was uncovered.[28]
Photoshopped pictures of Mario with a green tint have been presented as evidence of Luigi being playable, but no one has been able to accomplish this feat in the game. Nintendo has consistently denied Luigi's playability, and never commented on the meaning of the supposed "L is Real 2041" except for the April Fool's Day 1998 issue of Nintendo Power. In this issue, the "April News Briefs" section says that the cryptic phrase will be discussed on page 128, but the magazine only has 106 pages.[29] The "April News Briefs" section also featured a facetious article entitled "Luigi 64," commenting humorously on the rumors.[29] Luigi is a playable character in the enhanced Nintendo DS remake, Super Mario 64 DS. Among other Mario references, the suspicious texture reappears in The Legend of Zelda: Ocarina of Time, a game based on a heavily modified version of the Super Mario 64 engine. A similar blurry message is found in the Bone Yard in Luigi's Mansion.
[edit]
Notes and references
? Super Mario 64. IGN (2006-01-01). Retrieved on 2006-10-29.
? Perry, Doug. Super Mario 64. IGN. Retrieved on 2006-10-22.
? Berghammer, Billy (2006-09-15). Will Wii Be Disappointed Again?. Game Informer. Retrieved on 2006-10-22.
? Mario Sales Data. gamecubicle.com. Retrieved on 2006-02-10.
? 5.0 5.1 15 Most Influential Games of All Time. gamespot.com. Retrieved on 2006-07-03.
? N64 Reader Tributes: Super Mario 64. ign.com. Retrieved on 2006-10-21.
? 7.0 7.1 7.2 7.3 The Essential 50 Part 36: Super Mario 64. 1UP.com. Retrieved on 2006-10-21.
? The Best Video Games in the History of Humanity. filibustercartoons.com. Retrieved on 2006-02-11.
? 9.0 9.1 9.2 9.3 9.4 9.5 9.6 9.7 9.8 9.9 Nintendo (1996). Official Super Mario 64 Player's Guide (in English). Nintendo of America, Inc.. ASIN B000CQPA16.
? 10.0 10.1 10.2 10.3 Nintendo (1996). Super Mario 64 Instruction Booklet (in English). Nintendo of America, Inc.. NUS-NSME-USA.
? 11.0 11.1 11.2 11.3 (December 2001). "The Making of Mario 64: Giles Goddard Interview". NGC Magazine, vol 61.
? 12.0 12.1 12.2 (January 1996). "The Game Guys - (Spaceworld 1995)". Nintendo Power, vol 80. transcript
? Profile: Shigeru Miyamoto Chronicles of a Visionary, n-Sider
? (August 1995). "Miyamoto Interview". Nintendo Power, vol 75. transcript
? 15.0 15.1 (October 1996). "Miyamoto Interview". Nintendo Power, vol 89. transcript
? "Super Mario 64: From Japanese to English". The Mushroom Kingdom. [1]
? IGN's Top 100 Games. ign.com (2005). Retrieved on 2006-02-11.
? "Top 100 Games of All Time." GameInformer. August 2001: 36.
? (September 1996). "Super Mario 64". Nintendo Power, vol 88. pp. 14-23.
? (June 1996). "N64 Exclusive". Nintendo Power, vol 85. pp. 16-17.
? 21.0 21.1 GameSpy's Top 50 Games of All Time. gamespy.com (July 2001). Retrieved on 2006-02-11.
? The Making of GoldenEye 007. zoonami.com (September 2, 2004). Retrieved on 2006-02-11.
? Shindou Super Mario 64 (Rumble Pak Vers.). IGN. Retrieved on 2006-10-22.
? Davies, Jonti. Shindou Super Mario 64. allgame. Retrieved on 2006-10-22.
? Gerstmann, Jeff (2004-11-19). Super Mario 64 DS review. Gamespot. Retrieved on 2006-10-22.
? Casamassina, Matt (2006-09-19). IGN's Nintendo Wii FAQ pp. 5. IGN. Retrieved on 2006-10-22.
? In Search of Luigi. ign.com (November 13, 1996). Retrieved on 2006-02-11.
? Luigi Still Missing. ign.com (November 20, 1996). Retrieved on 2006-02-11.
? 29.0 29.1 (April 1998). "April News Briefs." Nintendo Power, vol 107. pp.80-81.
[edit]
See also
List of best-selling computer and video games
List of Mario games by year
List of Nintendo 64 games
[edit]
External links
Coverage
The Making of Super Mario 64 by Andy Robinson
Super Mario 64 at the Internet Movie Database
Super Mario 64 at StrategyWiki (previously hosted by Wikibooks)
Interviews
GameSpot - Tim Schafer (LucasArts) comments - 1999
Other
Super Mario Wiki
Video games featuring Mario
Donkey Kong � Mario Bros. � Super Mario Bros. � The Lost Levels � Super Mario Bros. 2 � Super Mario Bros. 3 � Super Mario Land Super Mario World � Super Mario Land 2 � Super Mario 64 � Super Mario Sunshine � New Super Mario Bros. � Super Mario Galaxy
Supporting characters � Games by year � Games by platform � Games by genre
Retrieved from "http://localhost../../../art/a/r/0.html"
This text comes from Wikipedia the free encyclopedia. Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. For a complete list of contributors for a given article, visit the corresponding entry on the English Wikipedia and click on "History" . For more details about the license of an image, visit the corresponding entry on the English Wikipedia and click on the picture.