Brothers Grimm
Brothers Grimm
For information about the other uses of the name, see Brothers Grimm (disambiguation).
Wilhelm (left) and Jacob Grimm (right) from an 1855 painting by Elisabeth Jerichau-Baumann
The Brothers Grimm (Br�der Grimm, in their own words, not Gebr�der - for there were five surviving brothers, among them Ludwig Emil Grimm, the painter) were Jacob and Wilhelm Grimm, Hessian professors who were best known for publishing collections of folk tales and fairy tales,[1] and for their work in linguistics, relating to how the sounds in words shift over time (Grimm's Law).
Contents
1 Biography
2 The Tales
3 Linguistics
4 Miscellaneous
5 Selection of fairy tales by the Brothers Grimm
6 References
6.1 Texts and recordings
6.2 Other
[edit]
Biography
Jakob Ludwig Carl Grimm and Wilhelm Karl Grimm were born in 1785 and 1786, respectively, in Hanau near Frankfurt in Hesse. They were among the six children of a prosperous lawyer; their early childhood was spent in the countryside, in what has been described as an "idyllic" state. When the eldest brother Jacob was eleven years old, however, their father died, and the family moved into a cramped urban residence. Two years later, the children's grandfather also died, leaving the children and their mother to struggle in reduced circumstances. (Modern psychologists have argued that this harsh family background influenced the ways the Brothers Grimm would interpret and present their tales. The Brothers tended to idealize and excuse fathers, leaving a predominance of female villains in the tales�the infamous evil stepmothers.) [2]
The two brothers were educated at the Friedrichs-Gymnasium in Kassel and later both read law at the University of Marburg. They were in their early twenties when they began the linguistic and philological studies that would culminate in both Grimm's Law and their collected editions of fairy and folk tales. Though their collections of tales became immensely popular, they were essentially a byproduct of the linguistic research which was the Brothers' primary goal.
In 1830, they formed a household in G�ttingen where they were to become professors.
In 1837, the Brothers Grimm joined five of their colleague professors at the University of G�ttingen to protest against the abolition of the liberal constitution of the state of Hanover by King Ernest Augustus I of Hanover a reactionary son of George III of England. This group came to be known in the German states as Die G�ttinger Sieben (The G�ttingen Seven). The two, along with the five others, protested against the abrogation. For this, the professors were fired from their university posts and some even deported--including Jakob. Jakob settled in Kassel, outside Ernst's realm, and Wilhelm joined him there, both staying with their brother Ludwig. However, the next year, the two were invited to Berlin by the King of Prussia, and both settled there.[1]
Graves of the Brothers Grimm in the St Matth�us Kirchhof Cemetery in Sch�neberg, Berlin.
Wilhelm died in 1859; his elder brother Jacob died in 1863. They are buried in the St. Matth�us Kirchhof Cemetery in Sch�neberg, Berlin. The Grimms helped foment a nationwide democratic public opinion in Germany and are cherished as the progenitors of the German democratic movement, whose revolution of 1848/1849 was crushed brutally by the Kingdom of Prussia, where there was established a constitutional monarchy.
[edit]
The Tales
The Brothers Grimm began collecting folk tales[3] around 1807, in response to a wave of awakened interest in German folklore that followed the publication of Ludwig Achim von Arnim and Clemens Brentano's folksong collection Des Knaben Wunderhorn ("The Boy's Magic Horn"), 1805-8. By 1810 the Grimms produced a manuscript collection of several dozen tales, which they had recorded by inviting storytellers to their home and transcribing what they heard. Although it is often believed that they took their tales from peasants, many of their informants were middle-class or aristocratic, recounting tales they had heard from their servants, and several of the informants were of Huguenot ancestry and told tales French in origin.[4]
In 1812, the Brothers published a collection of 86 German fairy tales in a volume titled Kinder- und Hausm�rchen ("Children's and Household Tales"). They published a second volume of 70 stories in 1814 ("1815" on the title page), which together make up the first edition of the collection, containing 156 stories. A second edition followed in 1819-22, expanded to 170 tales. Five more editions were issued during the Grimms' lifetimes,[5] in which stories were added or subtracted, until the seventh edition of 1857 contained 211 tales. Many of the changes were made in light of unfavorable reviews, particularly those that objected that not all the tales were suitable for children, despite the title.[6] They were also criticized for being insufficiently German; this not only affected the tales they included, but their language as they changed "Fee" (fairy) to an enchantress or wise woman, every prince to a king's son, every princess to a king's daughter.[7] (It has long been recognized that some of these later-added stories were derived from printed rather than oral sources.) [8]
These editions, equipped with scholarly notes, were intended as serious works of folklore. The Brothers also published the Kleine Ausgabe or "small edition," containing a selection of 50 stories expressly designed for children (as opposed to the more formal Grosse Ausgabe or "large edition"). Ten printings of the "small edition" were issued between 1825 and 1858.
The Grimms were not the first to publish collections of folktales. The 1697 French collection by Charles Perrault is the most famous, though there were various others, including a German collection by Johann Karl August Mus�us published in 1782-7. The earlier collections, however, made little pretense to strict fidelity to sources. The Brothers Grimm were the first workers in this genre to present their stories as faithful renditions of the kind of direct folkloric materials that underlay the sophistications of an adapter like Perrault. In so doing, the Grimms took a basic and essential step toward modern folklore studies, leading to the work of folklorists like Peter and Iona Opie[9] and others.
A century and a half after the Grimms began publishing, however, a sweeping, skeptical, and highly critical re-assessment disproved the Grimms' basic claims about their work.[10] The Brothers did not in fact use exclusively German sources for their collection; far from maintaining fidelity to those sources, they rewrote and revised and adapted their stories, just as Perrault and their other predecessors had done. The different printed versions of the tales display the latter fact; the 1810 manuscripts, published in 1924, 1927, and 1974, accentuate the Brothers' consistent habit of changing and adapting their original materials. The irony is that the Brothers Grimm helped create a serious scholarly discipline that they themselves never practiced.
It should be noted that the Grimms' method was common in their historical era. Arnim and Brentano edited and adapted the folksongs of Des Knaben Wunderhorn; in the early 1800s Brentano collected folktales in much the same way as the Grimms.[11] The good academic practices violated by these early researchers had not yet been codified in the period in which they worked. The Grimms have been criticized for a basic dishonesty, for making false claims about their fidelity�for saying one thing and doing another;[12] whether and to what degree they were deceitful, or self-deluding, is perhaps an open question.
[edit]
Linguistics
In the very early 19th century, the time in which the Brothers Grimm lived, the Holy Roman Empire had just met its fate, and Germany as we know it today did not yet exist; it was basically an area of hundreds of principalities and small or mid-sized countries. The major unifying factor for the German people of the time was a common language. There was no significant German literary history. So part of what motivated the Brothers in their writings and in their lives was the desire to help create a German identity.
Less well known to the general public outside Germany is the Brothers' work on a German dictionary, the Deutsches W�rterbuch. Indeed, the Deutsches W�rterbuch was the first major step in creating a standardized "modern" German language since Martin Luther's translation of the Bible to German; being very extensive (33 volumes, weighing 84 kg) it is still considered the standard reference for German etymology.
The brother Jacob is recognized for enunciating Grimm's law, Germanic Sound Shift, that was first observed by the Danish philologist Rasmus Christian Rask. Grimm's law was the first non-trivial systematic sound change ever to be discovered.
[edit]
Miscellaneous
Between 1990 and the 2002 introduction of the euro currency in Germany, the Grimms were depicted on the 1000 Deutsche Mark note�the largest available denomination.
The 1962 film The Wonderful World of the Brothers Grimm shows a fictionalized account of the conflict in the brothers' lives between their tale collecting and their philological work, as well as dramatizing three tales.
2005 found the brothers portrayed in the Terry Gilliam movie The Brothers Grimm starring Matt Damon and Heath Ledger.
[edit]
Selection of fairy tales by the Brothers Grimm
The Almond Tree
The Blue Light
The Valiant Little Tailor
Brother and Sister
Cinderella
The Bremen Town Musicians
The Elves and the Shoemaker
The Fisherman and His Wife
The Five Servants
The Frog Prince
The Gallant Sailor
The Golden Bird
The Golden Goose
The Goose Girl
The Grateful Beasts
Hansel and Gretel
Iron John
Jorinde and Joringel
The Juniper Tree
King Thrushbeard
The Little Peasant
Little Red Riding Hood
Mother Hulda
Old Sultan
The Pied Piper of Hamelin
Rumpelstiltskin
Rapunzel
The Raven
The Salad
Simeli mountain
Six Soldiers of Fortune
The Six Swans
(Sleeping Beauty) Briar Rose
Snow White
Snow White and Rose Red
The Spirit in the Bottle
The Three Little Men in the Woods
The Three Spinners
Tom Thumb
The Twelve Brothers
The Twelve Dancing Princesses
The Water of Life
The White Snake
The Wonderful Musician
The Brothers Grimm (1982). Fairy Tales. Julian Messner. ISBN 0-671-45648-2.
[edit]
References
? Jack Zipes, The Brothers Grimm: From Enchanted Forests to the Modern World, New York, Routledge & Kegan Paul, 1988; Palgrave MacMillan, 2002.
? Ian Alister and Christopher Hauke, eds., Contemporary Jungian Analysis, London, Routledge, 1998; pp. 216-19.
? James M. McGlathery, ed., The Brothers Grimm and Folktale, Champaigne, University of Illinois Press, 1988.
? Jack Zipes, When Dreams Came True: Classical Fairy Tales and Their Tradition, p 69-70 ISBN 0-415-92151-1
? Two volumes of the second edition were published in 1819, with a third volume in 1822. The third edition appeared in 1837; fourth edition, 1840; fifth edition, 1843; sixth edition, 1850; seventh edition, 1857. All were of two volumes, except for the three-volume second edition. Donald R. Hettinga, The Brothers Grimm: Two Lives, One Legacy, New York, Clarion Books, 2001; p. 154.
? Maria Tatar, The Hard Facts of the Grimms' Fairy Tales, p. 15-17, ISBN 0-691-06722-8
? Maria Tatar, The Hard Facts of the Grimms' Fairy Tales, p. 31, ISBN 0-691-06722-8
? Kathleen Kuiper, Merriam-Webster's Encyclopedia of Literature, Springfield, MA, Merriam-Webster, 1995, p. 494; Valerie Paradiz, Clever Maids: The Secret History of the Grimm Fairy Tales, New York, Basic Books, 2005, p. xii. One example: the tale "All Fur," Allerleirauh, in the 1857 collection derives from Carl Nehrlich's 1798 novel Schilly. Laura Gonzenbach, Beautiful Angiola: The Great Treasury of Sicilian Folk and Fairy Tales, London, Routledge, 2003; p. 345.
? Peter and Iona Opie, The Classic Fairy Tales, London, Oxford University Press, 1974, is the most famous of their many works in the field.
? John M. Ellis, One Fairy Story Too Many: The Brothers Grimm and Their Tales, Chicago, University of Chicago Press, 1983.
? Ellis, One Fairy Story too Many, pp. 2-7.
? Ellis, pp. 37 ff.
[edit]
Texts and recordings
Household Tales by the Brothers Grimm, translated by Margaret Hunt (This site is the only one to feature all of the Grimms' notes translated in English along with the tales from Hunt's original edition. Andrew Lang's introduction is also included.)
Grimm's Fairy Tales, available freely at Project Gutenberg
Grimm's household tales, available freely at Project Gutenberg. Translated by Margaret Hunt.
Brothers Grimm - Fairy Tales Audiobooks
Recording of 63 Fairy Tales by the Brothers Grimm at LibriVox.org
[edit]
Other
The Museum of the Brothers Grimm in Kassel, Germany
Grimm's M�rchen tiefenpsychologisch gedeutet by Dr. Eugen Drewermann (in German)
Jacob Grimm at the Internet Movie Database
Wilhelm Grimm at the Internet Movie Database
The Juniper Tree a film by Will Wright
'Grimm Reality' a film based on the unexpurgated tales
'Cult Grimm' a short 'movie' containing a Grimm tale
Grimmstories.com 40 Grimm's Fairy Tales available freely in English, German and Dutch
Retrieved from "http://localhost../../../art/a/w/1.html"
This text comes from Wikipedia the free encyclopedia. Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. For a complete list of contributors for a given article, visit the corresponding entry on the English Wikipedia and click on "History" . For more details about the license of an image, visit the corresponding entry on the English Wikipedia and click on the picture.