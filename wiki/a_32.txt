Congo River
Congo River
Congo
Image of Kinshasa and Brazzaville, taken by NASA; the Congo River is visible in the center of the photograph
Mouth
Atlantic Ocean
Basin countries
Democratic Republic of the Congo, Central African Republic, Republic of the Congo
Length
4,667 km (2,900 mi)
Avg. discharge
41,800 m�/s (1,476,376 ft�/s)
Basin area
3,680,000 km� (1,420,848 mi�)
The Congo River (formerly known as Zaire River) is the largest river in Western Central Africa. Its overall length of 4,374 km (2,718 mi.) makes it the second longest in Africa (after the Nile). The river and its tributaries flow through the second largest rain forest area in the world,[1] second only to the Amazon Rainforest in South America. The river also has the second-largest flow in the world, behind the Amazon, and the second-largest watershed of any river, again trailing the Amazon; its watershed is slightly larger than that of the Mississippi River. Because large sections of the river basin lie above and below the equator, its flow is stable, as there is always at least one river experiencing a rainy season.[2] The Congo gets its name from the ancient Kingdom of Kongo which inhabited the lands at the mouth of the river. The Democratic Republic of the Congo and the Republic of the Congo, both countries lying along the river's banks, are named after it. Between 1971 and 1997 the government of then-Zaire called it the Zaire River.
The sources of the Congo are in the highlands and mountains of the East African Rift, as well as Lake Tanganyika and Lake Mweru, which feed the Lualaba River, which then becomes the Congo below Boyoma Falls. The Chambeshi River in Zambia is generally taken as the source of the Congo in line with the accepted practice worldwide of using the longest tributary, as with the Nile River.
The Congo flows generally west from Kisangani just below the falls, then gradually bends southwest, passing by Mbandaka, joining with the Ubangi River, and running into the Pool Malebo (Stanley Pool). Kinshasa (formerly L�opoldville) and Brazzaville are on opposite sides of the river at the Pool, then the river narrows and falls through a number of cataracts in deep canyons (collectively known as the Livingstone Falls), running by Matadi and Boma, and into the sea at the small town of Muanda.
Contents
1 History of exploration
2 Economic importance
3 Tributaries
4 See also
5 Geological history
6 Notes
7 References
8 External links
[edit]
History of exploration
The mouth of the Congo was visited in 1482 by the Portuguese Diogo C�o, and in 1816 by a British expedition under James Kingston Tuckey went up as far as Isangila. Henry Morton Stanley was the first European to navigate along the river's length and report that the Lualaba was not a source of the Nile as had been suggested.
[edit]
Economic importance
Nearly the entire Congo is readily navigable, and with railways now bypassing the three major falls, much of the trade of central Africa passes along it, including copper, palm oil (as kernels), sugar, coffee, and cotton. The river is also potentially valuable for hydroelectric power, and the Inga Dams below Pool Malebo are first to exploit the river.
In February 2005, South Africa's state-owned power company, Eskom, announced a proposal to increase the capacity of the Inga dramatically through improvements and the construction of a new hydroelectric dam. The project would bring the maximum output of the facility to 40 GW, twice that of China's Three Gorges Dam. [2]
[edit]
Tributaries
Sorted in order from the mouth heading upstream.
Inkisi
Nzadi
Nsele (south side of Pool Malebo)
Bombo
Kasai (between Fimi and Congo, known as Kwa)
Fimi
Kwango
Sankuru
Likouala
Sangha
Ubangi
Giri
Uele
Mbomou
[edit]
See also
Congo (disambiguation)
Hydrology transport model
Portage railway
[edit]
Geological history
In the Mesozoic before continental drift opened the South Atlantic Ocean, the Congo was the upper part of a river roughly 12,000 km (7,500 miles) long which flowed west across the parts of Gondwanaland which are now Africa and South America: see Longest rivers#Amazon-Congo.
[edit]
Notes
? The Rainforest Foundation (June 21, 2006). A fresh step towards the first indigenous rights law in Republic of Congo.
? [1]
[edit]
References
H. Winternitz, East Along the Equator: A Journey up the Congo and into Zaire (1987)
[edit]
External links
Bibliography on Water Resources and International Law See Congo (or Za�re) River. Peace Palace Library
Information and a map of the Congo's watershed
Map of the Congo River basin at Water Resources eAtlas
Retrieved from "http://localhost../../art/3/0.html"
This text comes from Wikipedia the free encyclopedia. Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. For a complete list of contributors for a given article, visit the corresponding entry on the English Wikipedia and click on "History" . For more details about the license of an image, visit the corresponding entry on the English Wikipedia and click on the picture.