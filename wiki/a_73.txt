William I of England
William I of England
William I the Conqueror
King of the English; Duke of Normandy
The Duke of Normandy in the Bayeux Tapestry
Reign
25 December 1066 � 9 September 1087
Coronation
25 December 1066
Predecessor
Harold II
Normandy: Robert II the Magnificent
Successor
William II Rufus
Normandy: Robert III Curthose
Consort
Matilda of Flanders (1031 � 1083)
Issue
Robert III Curthose
William II Rufus
Adela, Countess of Blois
Henry I Beauclerc
among others...
Father
Robert II the Magnificent
Mother
Herlette of Falaise
Born
1024-1028
Falaise, France
Died
9 September 1087
Convent of St. Gervais, Rouen
Burial
Saint-�tienne de Caen, France
William of Normandy (French: Guillaume de Normandie; c. 1028 � 9 September 1087) ruled as the Duke of Normandy from 1035 to 1087 and as King of England from 1066 to 1087. William invaded England, won a victory at the Battle of Hastings in 1066, and suppressed subsequent English revolts in what has become known as the Norman Conquest. No authentic portrait of William has been found but he was described as a muscular man, strong in every sense of the word, balding in front, and of regal dignity.
In the present nomenclature, William was Duke of Normandy as William II and King of England as William I. He is also known as William the Conqueror (Guillaume le Conqu�rant) and William the Bastard (Guillaume le B�tard).
Contents
1 Early life
2 Conquest of England
3 William's reign
4 Death, burial, and succession
5 Genealogy
6 Children of William and Matilda
7 Direct descent from William I to Elizabeth II
8 See also
9 Further reading
10 References
11 External links
[edit]
Early life
The sole son of Robert the Magnificent and Herleva, most likely the daughter of a local tanner named Fulbert, William was born illegitimate in Falaise, Normandy. The exact date of birth is uncertain, but is known to have been either in 1027 or 1028, and more likely in the autumn of the latter year.[1] He was the grandnephew of Queen Emma, wife of King Ethelred the Unready and later of King Canute.
William succeeded to his father's Duchy of Normandy at the young age of seven in 1035 and was known as Duke William II of Normandy (Fr. Guillaume II, duc de Normandie). He lost three guardians to plots to usurp his place. Count Alan of Brittany was a later guardian. King Henry I of France knighted him at the age of 15. By the time he turned 19 he was successfully dealing with threats of rebellion and invasion. With the assistance of King Henry, William finally secured control of Normandy by defeating the rebel Norman barons at Caen in the Battle of Val-�s-Dunes in 1047.
He married his cousin Matilda of Flanders, against the wishes of the pope in 1053 at the Cathedral of Notre Dame at Eu, Normandy (now in Seine-Maritime). At the time, William was 26 and Matilda was 22. Their marriage produced four sons and six daughters (see list below).
His half-brothers Odo of Bayeux and Robert, Count of Mortain played significant roles in his life. He also had a sister, Adelaide of Normandy.
[edit]
Conquest of England
Upon the death of William's cousin King Edward the Confessor of England (January 1066), William claimed the throne of England, asserting that the childless and purportedly celibate Edward had named him his heir during a visit by William (probably in 1052) and that Harold Godwinson, England's foremost magnate and brother-in-law of the late King Edward the Confessor, had reportedly pledged his support while shipwrecked in Normandy (c. 1064). Harold made this pledge while in captivity and was reportedly tricked into swearing on a saint's bones that he would give the throne to William. Even if this story is true, however, Harold made the promise under duress and so may have felt free to break it. More realistically, by the mid 1050s, Harold was effectively ruling England through the weak King Edward and was unlikely to surrender the throne to a foreign noble.
English Royalty
House of Normandy
William I
Children
Robert III Curthose, Duke of Normandy
William II Rufus
Adela, Countess of Blois
Henry I Beauclerc
The assembly of England's leading nobles known as the Witenagemot approved Harold Godwinson�s coronation which took place on 5 January 1066 making him King Harold II of England. In order to pursue his own claim, William obtained the support of the Pope Alexander II for his cause. He assembled a Norman invasion fleet of around 600 ships and an army of 7000 men. He landed at Pevensey in Sussex on 28 September 1066 and assembled a prefabricated wooden castle (Motte-and-bailey) near Hastings as a base. This was a direct provocation to Harold Godwinson as this area of Sussex was Harold's own personal estate, and William began immediately to lay waste to the land. It may have prompted Harold to respond immediately and in haste rather than await reinforcements in London.
King Harold Godwinson was in the north of England and had just defeated another rival, Harald III of Norway, supported by his own brother Tostig. He marched an army of similar size to William's 250 miles in 9 days to challenge him at the crucial battle of Senlac, which later became known as the Battle of Hastings. This took place on 14 October 1066. According to some accounts, perhaps based on an interpretation of the Bayeux Tapestry commemorating the Norman victory, Harold was allegedly killed by an arrow through the eye, and the English forces fled giving William victory.
This was the defining moment of what is now known as the Norman Conquest. Unable to enter London, William travelled to Wallingford, was welcomed in by Wigod who supported his cause. This is where the first submissions took place including that of the Archbishop of Canterbury.[2] The remaining Anglo-Saxon noblemen surrendered to William at Berkhamsted, Hertfordshire and he was acclaimed King of England there. William was then crowned on 25 December 1066 in Westminster Abbey.
Although the south of England submitted quickly to Norman rule, resistance continued, especially in the North for six more years until 1072. Harold's illegitimate sons attempted an invasion of the south-west peninsula. Uprisings occurred in the Welsh Marches and at Stafford. Separate attempts at invasion by the Danes and the Scots also occurred. William's defeat of these led to what became known as the harrying of the North, in which Northumbria was laid waste as revenge and to deny his enemies its resources. The last serious resistance came with the Revolt of the Earls in 1075. It is estimated that one-fifth of the population of England was killed during these years by war, massacre, and starvation.
[edit]
William's reign
William initiated many major changes. In 1085, in order to ascertain the extent of his new dominions and maximize taxation, William commissioned the compilation of the Domesday Book, a survey of England's productive capacity similar to a modern census. He also ordered many castles, keeps, and mottes, among them the Tower of London, to be built across England to ensure that the rebellions by the English people or his own followers would not succeed. His conquest also led to Norman replacing English as the language of the ruling classes for nearly 300 years.
The signatures of William I and Matilda are the first two large crosses on the Accord of Winchester from 1072.
William is said to have deported some of the Anglo-Saxon land owning classes into slavery through Bristol.[citation needed] Many of the latter ended up in Umayyad Spain and Moorish lands. Ownership of nearly all land in England and titles to religious and public offices were given to Normans. Many surviving Anglo-Saxon nobles emigrated to other European kingdoms.
[edit]
Death, burial, and succession
William died at the age of 59, at the Convent of St Gervais, near Rouen, France, on 9 September 1087 from abdominal injuries received from his saddle pommel when he fell off a horse at the Siege of Mantes. William was buried in the church of St. Stephen in Caen, Normandy. In a most unregal postmortem, his corpulent body would not fit in the stone sarcophagus, and burst after some unsuccessful prodding by the assembled bishops, filling the chapel with a foul smell and dispersing the mourners. [3]
William was succeeded in 1087 as King of England by his younger son William Rufus and as Duke of Normandy by his elder son Robert Curthose. This led to the Rebellion of 1088. His youngest son Henry also became King of England later, after William II died without a child to succeed him.
[edit]
Genealogy
Diagram based on the information found on Wikipedia
Every English monarch down to Queen Elizabeth II is a direct descendant of William the Conqueror as well as Alfred the Great.
[edit]
Children of William and Matilda
Some doubt exists over how many daughters there were. This list includes some entries which are obscure.
Robert Curthose (c. 1054�1134), Duke of Normandy, married Sybil of Conversano, daughter of Geoffrey of Conversano
Adeliza (or Alice) (c. 1055�?), reportedly betrothed to Harold II of England (Her existence is in some doubt.)
Cecilia (or Cecily) (c. 1056�1126), Abbess of Holy Trinity, Caen
William Rufus (1056�1100), King of England
Richard (1057-c. 1081), killed by a stag in New Forest
Adela (c. 1062�1138), married Stephen, Count of Blois
Gundred (c. 1063�1085), married William de Warenne (c. 1055�1088) Some scholars question whether Gundred was an illegitimate child of William I or merely a step-daughter, foundling or adopted daughter. See discussion pages for further information.
Agatha (c. 1064�c. 1080), betrothed to (1) Harold of Wessex, (2) Alfonso VI of Castile
Constance (c. 1066�1090), married Alan IV Fergent, Duke of Brittany; poisoned, possibly by her own servants
Matilda (very obscure, her existence is in some doubt)
Henry Beauclerc (1068�1135), King of England, married (1) Edith of Scotland, daughter of Malcolm III, King of Scotland, (2) Adeliza of Louvain
[edit]
Direct descent from William I to Elizabeth II
[edit]
See also
Rankilor
[edit]
Further reading
David Bates, William the Conqueror (1989) ISBN 978-0-7524-1980-0
David C. Douglas, William the Conqueror; the Norman Impact Upon England (1964) [no ISBN]
David Howarth, 1066 The Year of the Conquest (1977) ISBN 0-14-005850-5
H. F. M. Prescott, Son of Dust (1932)
Anne Savage, The Anglo-Saxon Chronicles ISBN 978-1-85833-478-3, pub.CLB, 1997
[edit]
References
? The official web site of the British Monarchy puts his birth at "around 1028", which may reasonably be taken as definitive.
The frequently encountered date of 14 October 1024 is likely spurious. It was promulgated by Thomas Roscoe (1791-1871) in his 1846 biography The life of William the Conqueror. The year 1024 is apparently calculated from the fictive deathbed confession of William recounted by Ordericus Vitalis (who was about twelve when the Conqueror died); in it William allegedly claimed to be about sixty-three or four years of age at his death in 1087. The birth day and month are suspiciously the same as those of the Battle of Hastings. This date claim, repeated by other Victorian historians (e.g. Jacob Abbott), has been entered unsourced into the LDS genealogical database, and has found its way thence into countless personal genealogies. Cf. The Conqueror and His Companions by J.R. Planch�, Somerset Herald. London: Tinsley Brothers, 1874.
? http://www.berkshirehistory.com/villages/wallingford.html
? http://historyhouse.com/in_history/william/
[edit]
External links
History of William I's life and reign. Official web site of the British Monarchy
Documentary - The Making of England: William the Conqueror
William the Conqueror. by E. A. Freeman (1823-1892). Ebook published via Gutenberg Project.
Illustrated biography of William the Conqueror
William I of England At Find A Grave
History House: William the Conqueror
Preceded by:
Edgar �theling
King of England
1066�1087
Succeeded by:
William II
Preceded by:
Robert the Magnificent
Duke of Normandy
1035�1087
Succeeded by:
Robert Curthose
Monarchs of England
something
Alfred � Edward the Elder � �lfweard � Athelstan � Edmund I � Edred � Edwy � Edgar I � Edward the Martyr � Ethelred � Sweyn I*� � Edmund II � Canute*� � Harthacanute* � Harold I � Edward the Confessor � Harold II � Edgar II � William I � William II � Henry I � Stephen � Matilda � Henry II � Richard I � John � Henry III � Edward I � Edward II � Edward III � Richard II � Henry IV � Henry V � Henry VI � Edward IV � Edward V � Richard III � Henry VII � Henry VIII� � Edward VI� � Lady Jane Grey � � Mary I� � Elizabeth I� � James I�� � Charles I�� � Interregnum � Charles II�� � James II�� � William III��� and Mary II�� (as co-monarchs William & Mary) � William III��� (own reign) � Anne��
* also Monarch of Denmark � � also Monarch of Norway � �also Monarch of Ireland � � also Monarch of Scotland � � also Stadtholder of Holland, Zeeland, Utrecht, Gelderland, Overijssel and Drenthe�
Retrieved from "http://localhost../../../art/a/h/1.html"
This text comes from Wikipedia the free encyclopedia. Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. For a complete list of contributors for a given article, visit the corresponding entry on the English Wikipedia and click on "History" . For more details about the license of an image, visit the corresponding entry on the English Wikipedia and click on the picture.