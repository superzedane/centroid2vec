import centroid2vec
import collections
import gensim.models

files = centroid2vec.listFiles('./wiki/')
files.sort()

allsentences = []

# load pretrained Stanford Glove vector from file but remove some words such as stopwords and etc
model = centroid2vec.getGloveModel(300, restrict_vocabs=True)
model.save('experiment_7_nn_300_NV.bin')

# reduce time to load Model
# model = gensim.models.KeyedVectors.load('experiment_7_nn_300_NV.bin')

for filename in files:
    
    # read text from file
    text = centroid2vec.getTextFromFile(filename)

    # spilt to sentences
    sentences = centroid2vec.cleanSetences(text)

    # clean words, only nouns
    tokens = centroid2vec.cleanDoc(text)
    tokens = centroid2vec.findNoun(tokens)
    
    # create counter words
    vocab_count = centroid2vec.countWords(tokens)

    # remove not significant words
    max_count = vocab_count.most_common(1)[0][1]
    rcount = 1
    if max_count>10:
        rcount = max_count // 5
    vocab_count = centroid2vec.removeCounter(vocab_count, rcount)
    vocab_count = collections.Counter(vocab_count)

    # remove words that don't exist in model
    centroid2vec.removeCounterNoVocab(model, vocab_count)
    
    if len(vocab_count) > 0:
        #finding centroids by using word in corpus
        vector = centroid2vec.findCentroid1(model, vocab_count)
        centroid1s = centroid2vec.findWordsFromVector(model,vector,1)

        #finding centroid by using words in a document only
        centroid4s = centroid2vec.findCentroid4(model,vocab_count)
        
        # print(centroids)
        print(filename + ';' + centroid2vec.getFirstLine(filename).rstrip() + ";" + vocab_count.most_common(1)[0][0] + ';' + centroid1s[0][0] + ';' + centroid4s + ";" + str(max_count))
        # vector = centroid2vec.findCentroid3(model,vocab_count)
        # pca = centroid2vec.calPCA(model, vocab_count)
        # centroid2vec.plotScatter(pca,vocab_count, centroid2vec.color())
        # centroid2vec.plotShow()
